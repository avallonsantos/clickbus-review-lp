// Applying initial datetime 

window.addEventListener('load', () => {
    document.getElementById('initial-datetime').value = new Date().toISOString()
})

class Review {
    // Declare "private" class attributes
    _form = document.getElementById('performance-evaluation')
    _reviewAnswerPage = document.getElementById('review-answers')
    _target = document.querySelector('.get-review')

    // Declare constructor
    constructor(evaluated = '', formData = []) {
        this.evaluated = evaluated
        if (!Array.isArray(formData)) {
            throw new Error('O conteúdo passado deve ser um array de objetos')
        } else {
            this.formData = formData
        }
    }

    // Getters

    getEvaluated() {
        return this.evaluated
    }

    getformContent() {
        return this.formData
    }


    handleServerResponse(response) {
        console.log(response)
        if (response.indexOf('[SUCCESS] ') > -1) {
            console.log('Successful submission sent')
	        this.showSuccessMessage()
        }

        if (response.indexOf('[ERROR] ') > -1) {
            alert('Houve um erro ao enviar o feedback. Feche esse alerta e recarregue a página para tentar novamente. Se o erro persistir, por favor avisar a equipe de People.')
        }
    }

    createRevision() {
        // Create required constants
        const reviewData = this.getformContent()
        const evaluated = this.getEvaluated()
        const reviewPage = document.createElement('div')

        // Apply required classes
        this._form.classList.add('opened')
        setTimeout(() => {
            this._form.classList.add('limited')
            document.getElementsByTagName('footer')[0].style.display = 'none'
        }, 600);

        // Apply name of colaborator

        document.querySelector('.evaluator-name').innerHTML = `Percepção do/a Colaborador/a: ${evaluated}`


        // Create revision page

        reviewData.forEach((element) => {
            reviewPage.innerHTML += `
        <div class="revision-section">
          <h3>${element.name}</h3>
          <div class="answer">${element.values.reviewText}</div>
          <textarea class="justification" disabled>${element.values.justification}</textarea>
        </div>
      `
        })

        // Append content on target

        this._target.appendChild(reviewPage)
    }

    showSuccessMessage() {
	    // Create success element
	    const success = document.createElement('div')
	    success.setAttribute('class', 'success-message hidden-content bottom')
	    success.innerHTML = '<div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-7"><img src="https://static.clickbus.com/live/ClickBus/landing-pages/evaluation/img/success.svg" alt="Success icon"></div><div class="align-self-start align-self-md-center col-sm-12 col-md-5"><div class="success-content"><h3>Avaliação <br> enviada </h3> <div class="evaluator-finish">' + document.querySelector('.evaluator').innerText + '</div><div class="actions"><button class="button" id="reload">Realizar próximo feedback</button></div></div> </div></div></div>'

	    // Append success element
	    document.getElementById('success-page').appendChild(success)

	    // Apply confirmed class - Review Section
	    setTimeout(() => {
		    this._reviewAnswerPage.classList.add('confirmed')
	    }, 2000)

	    setTimeout(() => {
		    this._reviewAnswerPage.style.display = 'none'
	    }, 2600);

	    document.getElementById('reload').addEventListener('click', () => {
	    	const linkReload = this._form.getAttribute('data-link-reload')
		    window.open(linkReload, '_blank')

	    })
    }


    finalizeEvaluation() {
        // Applying hour of submission
        document.getElementById('final-datetime').value = new Date().toISOString()

        // Apply required css classes
        document.getElementById('confirm').classList.add('loading')
        document.getElementById('restart').classList.add('closed')

        // Sending data to Google

        google.script.run.withSuccessHandler(this.handleServerResponse.bind(this)).storeFormSubmission(this._form)

    }

    restartEvaluation() {
        document.querySelector('.header-review').scrollIntoView()
        window.scrollTo({
            top: 0,
            left: 0
        })

        // Apply required classes
        this._form.classList.remove('opened')
        this._form.classList.remove('limited')
        document.getElementsByTagName('footer')[0].style.display = 'block'

        // Removing content from Review Section
        this._target.innerHTML = null

        // Removing classes from elements
        document.querySelector('button.submit').classList.remove('loading')
        document.getElementById('confirm').classList.remove('loading')
        document.getElementById('restart').classList.remove('closed')

    }
}

document.getElementById('avaliado').addEventListener('change', function () {
    if (this.value !== '') {
        window, scrollTo({
            left: 0,
            top: document.getElementById('customer-focus').offsetTop + 40,
            behavior: 'smooth'
        })
    }

})

// Creating review Class

const review = new Review()

// Confirm sending of evaluation

document.getElementById('confirm').addEventListener('click', () => {
    review.finalizeEvaluation()
})

// Restarting evaluation

document.getElementById('restart').addEventListener('click', () => {
    review.restartEvaluation()
    delete review.evaluated
    delete review.formData
})

// Get form element

const form = document.getElementById('performance-evaluation')

// Create new Pristine object
const pristineValidation = new Pristine(form)

// Get submit from form
form.addEventListener('submit', (e) => {
    e.preventDefault()

    // If form is valid
    let valid = pristineValidation.validate()

    // If validation is false, then
    if (valid === false) {
        let firstElementValidatedOffset = document.querySelector('.has-danger').offsetParent.offsetTop

        window.scrollTo({
            left: 0,
            top: firstElementValidatedOffset
        })

    }

    // If validate is true, then
    if (valid === true) {

        // Create an empty array that is going to store the values fields
        const formObjectData = []

        // Get the evaluated name
        const evaluated = document.getElementById('avaliado').value

        // Get all sections of inputs and create a iteration to get the theirs values
        let elements = document.querySelectorAll('.input-section')
        elements.forEach((element) => {
            let inputs = element.querySelectorAll('input[type="radio"]')
            inputs.forEach((input) => {
                if (input.checked === true) {
                    // Get the chacked values and push then into array
                    formObjectData.push({
                        name: input.name,
                        values: {
                            reviewText: input.getAttribute('data-value-text'),
                            justification: element.getElementsByTagName('textarea')[0].value
                        }
                    })
                }
            })
        })

        // Applying loding class on button

        document.querySelector('button.submit').classList.add('loading')

        // Create new Review object and create new revision after 2 seconds

        review.evaluated = evaluated
        review.formData = formObjectData

        setTimeout(() => review.createRevision(), 2000)
    }
})
