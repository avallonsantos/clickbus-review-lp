const gulp = require('gulp')
const sass = require('gulp-sass')
const concat = require('gulp-concat')
const browserSync = require('browser-sync').create()
const postcss = require('gulp-postcss')
const cssnano = require('cssnano')
const terser = require('gulp-terser')
const rename = require('gulp-rename')
const autoprefixer = require('autoprefixer')
const babel = require('gulp-babel')
const plumber = require('gulp-plumber')

const sources = {
  styles: {
    src: './src/scss/**/*.scss',
    dist: './dist/css'
  },
  scripts: {
    src: [
      './node_modules/pristinejs/dist/pristine.js',
      './src/js/**/*.js'
    ],
    dist: './dist/js'
  }
}

let css = () => {
  return gulp
         .src(sources.styles.src)
         .pipe(sass())
         .pipe(gulp.dest('./src/css/'))
         .pipe(postcss([
           autoprefixer([{ browsers: ['IE 8', 'IE 9', 'last 5 versions', 'Firefox 14', 'Opera 11.1'] }]),
           cssnano()
         ]))
         .pipe(rename('clickbusreview.min.css'))
         .pipe(gulp.dest(sources.styles.dist))
         .pipe(browserSync.reload({
           stream:true
         }));
}

let js = () => {
  return gulp
         .src(sources.scripts.src)
         .pipe(concat('app.js'))
         .pipe(babel({
            presets: [
              ['@babel/env', {
                modules: false
              }]
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties'
            ]
          }))
         .pipe(terser())
         .pipe(rename('clickbusreview.min.js'))
         .pipe(gulp.dest(sources.scripts.dist))
         .pipe(browserSync.reload({
           stream:true
         }));
}

let watch = () => {
  browserSync.init({
    server: {
      baseDir: './'
    }
  })

  gulp.watch(sources.styles.src, css);
  gulp.watch(sources.scripts.src, js)
  gulp.watch("*.html").on('change', browserSync.reload);
}

exports.watch = watch
exports.css = css
exports.js = js

const build = gulp.parallel(css, js, watch)

gulp.task('default', build)
